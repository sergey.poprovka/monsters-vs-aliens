$('document').ready(function(){

    $('.addTarget').click(function(e){
        e.preventDefault()
        $('#target').val($(this).data('id'))
    });

    $('.addSource').click(function(e){
        e.preventDefault()
        $('#source').val($(this).data('id'))
    });

    $('.addAction').click(function(e){
        e.preventDefault()
        $('#action').val($(this).data('type'))
    });

});