<?php
    require 'vendor/autoload.php';

    use Classes\Character;

    session_start();

    if(isset($_POST['hit_source'])){
        $id = $_POST['hit_source'];
        $target = "hero";
        $source = $_POST['target'];

        if($_SESSION['step']%2 != 0){
            $target = "enemy";
            $source = $_POST['hit_source'];
            $id = $_POST['target'];
        }

        if($_POST['source_action'] == ""){
            switch($target){
                case "hero":
                    $victim = new \Classes\Hero($_SESSION['heroes'][$id]['hp'], $_SESSION['heroes'][$id]['mp']);
                    $victim->receive_hit(\Classes\Enemy::instance()->make_hit());
                    if($victim->getHealth() == 0){
                        unset($_SESSION['heroes'][$id]);
                    }else{
                        $_SESSION['heroes'][$id]['hp'] = $victim->getHealth();
                        $_SESSION['heroes'][$id]['mp'] = $victim->getMagic();
                    }
                    break;
                case "enemy":
                    $victim = new \Classes\Enemy($_SESSION['enemies'][$id]['hp'], $_SESSION['enemies'][$id]['mp']);
                    $victim->receive_hit(\Classes\Hero::instance()->make_hit());
                    if($victim->getHealth() == 0){
                        unset($_SESSION['enemies'][$id]);
                    }else {
                        $_SESSION['enemies'][$id]['hp'] = $victim->getHealth();
                        $_SESSION['enemies'][$id]['mp'] = $victim->getMagic();
                    }
                    break;
            }
        }else{
            // MAGIC HIT
            switch($target){
                case "hero":
                    $src = new \Classes\Enemy($_SESSION['enemies'][$source]['hp'], $_SESSION['enemies'][$source]['mp']);
                    $src->spend_magic($_POST['source_action']);
                    $victim = new \Classes\Hero($_SESSION['heroes'][$id]['hp'], $_SESSION['heroes'][$id]['mp']);
                    $victim->receive_hit(\Classes\Enemy::instance()->make_magic_hit($_POST['source_action']));
                    if($victim->getHealth() == 0){
                        unset($_SESSION['heroes'][$id]);
                    }else{
                        $_SESSION['enemies'][$source]['hp'] = $src->getHealth();
                        $_SESSION['enemies'][$source]['mp'] = $src->getMagic();
                        $_SESSION['heroes'][$id]['hp'] = $victim->getHealth();
                        $_SESSION['heroes'][$id]['mp'] = $victim->getMagic();
                    }
                    break;
                case "enemy":
                    $src = new \Classes\Hero($_SESSION['heroes'][$source]['hp'], $_SESSION['heroes'][$source]['mp']);
                    $src->spend_magic($_POST['source_action']);
                    $victim = new \Classes\Enemy($_SESSION['enemies'][$id]['hp'], $_SESSION['enemies'][$id]['mp']);
                    $victim->receive_hit(\Classes\Hero::instance()->make_magic_hit($_POST['source_action']));
                    if($victim->getHealth() == 0){
                        unset($_SESSION['enemies'][$id]);
                    }else {
                        $_SESSION['heroes'][$source]['hp'] = $src->getHealth();
                        $_SESSION['heroes'][$source]['mp'] = $src->getMagic();
                        $_SESSION['enemies'][$id]['hp'] = $victim->getHealth();
                        $_SESSION['enemies'][$id]['mp'] = $victim->getMagic();
                    }
                    break;
            }
        }

        $_SESSION['step'] += 1;
    }





    header('Location:index.php');
?>