<?php
    namespace Classes;

    use Classes\Character;

    class Hero extends Character{

        public static function instance(){
            return new self;
        }

        CONST spells = [
            'fire'=> ['cost'=>25, 'max_hit'=>60, 'min_hit'=>25],
            'ice'=> ['cost'=>35, 'max_hit'=>65, 'min_hit'=>15]
        ];

        public function spend_magic($type){
            $this->magic_power -= self::spells[$type]['cost'];
        }

        public function make_magic_hit($type){
            return rand(self::spells[$type]['min_hit'], self::spells[$type]['max_hit']);
        }
    }
?>