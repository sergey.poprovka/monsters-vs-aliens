<?php
    namespace Classes;

    use Classes\Character;

    class Enemy extends Character{

        public static function instance(){
            return new self;
        }

        CONST spells = [
            'beer'=> ['cost'=>15, 'max_hit'=>30, 'min_hit'=>5],
            'smoke'=> ['cost'=>45, 'max_hit'=>80, 'min_hit'=>10]
        ];

        public function spend_magic($type){
            $this->magic_power -= self::spells[$type]['cost'];
        }

        public function make_magic_hit($type){
            return rand(self::spells[$type]['min_hit'], self::spells[$type]['max_hit']);
        }
    }
?>