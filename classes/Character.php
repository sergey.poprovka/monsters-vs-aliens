<?php
    namespace Classes;

    class Character{

        public static function instance(){
            return new self;
        }

        protected $health;
        protected $magic_power;
        protected $max_hit;
        protected $min_hit;
        protected $status = "Alive";

        public function __construct($health = 100, $magic_power = 100, $max_hit = 90, $min_hit = 70){
            $this->health = $health;
            if($this->health == 0){
                $this->kill();
            }
            $this->magic_power = $magic_power;
            $this->max_hit = $max_hit;
            $this->min_hit = $min_hit;

        }

        public function getHealth()
        {
            return $this->health;
        }

        public function getMagic(){
            return $this->magic_power;
        }

        public function getStatus()
        {
            return $this->health != 0 ? $this->health : $this->status;
        }

        public function make_hit(){
            return rand($this->min_hit, $this->max_hit);
        }

        private function kill(){
            $this->health = 0;
            $this->status = "Dead";
        }

        public function receive_hit($amount){
            if($this->health - $amount <= 0){
                $this->kill();
            }else {
                $this->health -= $amount;
            }
        }

    }
?>