<?php

session_start();

include 'vendor/autoload.php';

use Classes\Hero;
use Classes\Enemy;

$heroes = [];
$enemies = [];

print_r($_SESSION);

if(!isset($_SESSION['heroes'])) {

    $_SESSION['step'] = 1;

    for ($i = 0; $i < 5; $i += 1) {
        $heroes[] = new Hero();
        $_SESSION['heroes'][$i]['hp'] = $heroes[$i]->getHealth();
        $_SESSION['heroes'][$i]['mp'] = $heroes[$i]->getMagic();
        $_SESSION['heroes'][$i]['spells'] = $heroes[$i]::spells;

        $enemies[] = new Enemy();
        $_SESSION['enemies'][$i]['hp'] = $enemies[$i]->getHealth();
        $_SESSION['enemies'][$i]['mp'] = $enemies[$i]->getMagic();
        $_SESSION['heroes'][$i]['spells'] = $enemies[$i]::spells;
    }
}else{
    foreach($_SESSION['heroes'] as $key=>$hero){
        $heroes[$key] = new Hero($_SESSION['heroes'][$key]['hp'], $_SESSION['heroes'][$key]['mp']);
    }
    foreach($_SESSION['enemies'] as $key=>$enemy){
        $enemies[$key] = new Enemy($_SESSION['enemies'][$key]['hp'], $_SESSION['enemies'][$key]['mp']);
    }
}

//$hero = new Hero();
//$enemy = new Enemy();

//while($enemy->getHealth() > 0){
//    $hit = $hero->make_hit();
//    $enemy->receive_hit($hit);
//    echo "<strong>Hero hitted: </strong>" . $hit . "<br>";
//    echo "<strong>Enemy's health: </strong>" . $enemy->getHealth(). "<br>";
//    echo "<hr>";
//}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Game</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/game.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="text-center">
        <?php
            if($_SESSION['step'] % 2 != 0){
                echo "Heroes turn";
            }else{
                echo "Enemies turn";
            }
        ?>
    </div>
    <div class="container-fluid">
        <?php
            if(count($heroes) && count($enemies)):
        ?>
        <div class="row">
            <div class="col-6 text-center">
                <h2 class="text-center <?=($_SESSION['step']%2 != 0) ? 'text-danger':''?>">My army</h2>
                <hr>
                <ul class="list-unstyled">
                    <?php
                        foreach($heroes as $hkey=>$hero){
                    ?>
                        <li class="mb-3">
                            <div class="row">
                                <div class="offset-4 col-2">
                                <a href="#" class="addSource" data-id="<?=$hkey?>">
                                    <img src="images/superhero.svg" width="45" />
                                    <div class="small text-danger">
                                        HP: <?=$hero->getHealth()?>
                                    </div>
                                    <div class="small text-info">
                                        MP: <?=$hero->getMagic()?>
                                    </div>
                                </a>
                                </div>
                                <div class="col-1">
                                    <span class="d-flex flex-column">
                                        <a href="#" class="addAction" data-type="fire"><img src="images/fire.svg" width="25" /></a>
                                        <a href="#" class="addAction" data-type="ice"><img src="images/ice.svg" width="25" /></a>
                                    </span>
                                </div>
                        </li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
            <div class="col-6 text-center">
                <h2 class="text-center <?=($_SESSION['step']%2 == 0) ? 'text-danger':''?>">Enemy army</h2>
                <hr>
                <ul class="list-unstyled">
                    <?php
                        foreach($enemies as $ekey=>$enemy){
                    ?>
                        <li class="mb-3">
                            <div class="row">
                                <div class="offset-4 col-2">
                                    <a href="#" class="addTarget" data-id="<?=$ekey?>">
                                        <img src="images/frankenstein.svg" width="45" />
                                        <div class="small text-danger">
                                            HP: <?=$enemy->getStatus()?>
                                        </div>
                                        <div class="small text-info">
                                            MP: <?=$enemy->getMagic()?>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-1">
                                    <span class="d-flex flex-column">
                                        <a href="#" class="addAction" data-type="beer"><img src="images/beer.svg" width="25" /></a>
                                        <a href="#" class="addAction" data-type="smoke"><img src="images/smoke.svg" width="25" /></a>
                                    </span>
                                </div>
                            </div>


                        </li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
        <?php
            else:
        ?>
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center">Game over</h1>
                    <div class="text-center">
                        <?=count($heroes) ? "Heroes" : "Enemies"?> won!
                    </div>
                </div>
            </div>
        <?php
            endif
        ?>
        <form method="post" action="hit.php">
            <input type="text" id="source" name="hit_source" placeholder="Source" />
            <input type="text" id="action" name="source_action" placeholder="Source action" />
            <input type="text" id="target" name="target" placeholder="Target" />
            <button type="submit" class="btn btn-success btn-lg">Hit!</button>
        </form>
        <hr>
        <div class="row mt-5 mb-5">
            <div class="col-12 text-center">
                <a href="end_game.php" class="btn btn-danger btn-lg">End game</a>
            </div>
        </div>
    </div>
</body>
</html>
